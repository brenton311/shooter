#ifndef MENU_HPP
#define MENU_HPP

#include <SFML/Graphics.hpp>
#include "WindowManager.hpp"

class Menu
{
public:
    virtual ~Menu(){}
    virtual void draw(WindowManager& wm) = 0;
    virtual void update(WindowManager& wm) = 0;
};

#endif // MENU_HPP
