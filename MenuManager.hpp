#ifndef MENUMANAGER_HPP
#define MENUMANAGER_HPP

#include "Menu.hpp"

class MenuManager
{
    public:
        static MenuManager& getInstance();
        void setMenu(Menu* menu);
        Menu* getCurrentMenu();
        ~MenuManager(){}
    private:
        Menu* m_menu;
        MenuManager(const MenuManager&);
        MenuManager(){}
};

#endif // MENUMANAGER_HPP
