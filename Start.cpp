/*
    Defines the Entry point for the application
*/
#include <iostream>
#include <GL/glew.h>
#include "GameManager.hpp"

int main()
{
    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
        std::cout << "Failed to initialize GLEW!" << std::endl;
        return -1;
    }

    if(!GLEW_VERSION_3_2)
    {
        std::cout << "OpenGL Version 3.2 is not supported!" << std::endl;
        std::cout << "Supported OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
        return -1;
    }

    GameManager gm;
    gm.run();
}
