#include "MainMenu.hpp"
#include <SFML/Graphics.hpp>
#include <iostream>

MainMenu::MainMenu()
{
    //ctor
}

MainMenu::~MainMenu()
{
    //dtor
}

void MainMenu::draw(WindowManager& wm)
{
    std::cout << "Main Menu!" << std::endl;
    sf::RectangleShape rect;
    rect.setFillColor(sf::Color::Blue);
    rect.setPosition(0, 0);
    wm.getWindow()->draw(rect);
}

void MainMenu::update(WindowManager& wm)
{

}
