#ifndef WINDOWMANAGER_HPP
#define WINDOWMANAGER_HPP

#include <SFML/Graphics.hpp>

class WindowManager
{
public:
    WindowManager();
    ~WindowManager();
    void swapBuffers();
    void handleEvents();
    sf::RenderWindow* getWindow();
    bool isActive();
    bool isOpen();
private:
    sf::RenderWindow m_window;
    bool m_active;
};

#endif // WINDOWMANAGER_HPP
