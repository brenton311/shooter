/*
    File: GameManager.cpp
    Handles the game updating, rendering and the game loop
*/
#include <SFML/System.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/Graphics.hpp>
#include <iostream>
#include "GameManager.hpp"
#include "RenderUtl.hpp"

sf::Texture img;
GLuint textures[1];
ObjData data;

GameManager::GameManager()
{
}

GameManager::~GameManager()
{

}


void GameManager::run()
{
    initGL();

    // Various clocks to time the tick and for other profiling uses
    sf::Clock clock, tickClock, drawClock, secondsClock;
    bool running = true;
    unsigned int numOfUpdates = 0;

    while(running)
    {
        // Always handle window events
        m_wm.handleEvents();
        running = m_wm.isOpen();

        // Only update every 50 milliseconds (20 times a second)
        if(clock.getElapsedTime() > sf::milliseconds(50))
        {
            /* Tick clock used to time how long,
               it takes to complete a loop. A loop
               should take 50 milliseconds */
            tickClock.restart();
            update();

            /* If the update did not take too
               long we still have time to render */
            if(tickClock.getElapsedTime() < sf::milliseconds(50))
            {
                drawClock.restart();
                draw();
                m_wm.swapBuffers();
                /*std::cout << "Took " << drawClock.getElapsedTime().asSeconds() * 1000
                    << " ms to draw!" << std::endl;*/
            }
            else
            {
                std::cout << "Not enough time to render!\nUpdate took: "
                          << tickClock.getElapsedTime().asSeconds() * 1000
                          << "ms." << std::endl;
            }

            //sf::sleep(sf::milliseconds(50) - clock.getElapsedTime());
            //clock.restart();
            std::cout << tickClock.restart().asSeconds() * 1000 << std::endl;

            numOfUpdates++;
            if(secondsClock.getElapsedTime().asSeconds() > 1)
            {
                std::cout << "Updates: " << numOfUpdates << "!" << std::endl;
                numOfUpdates = 0;
                secondsClock.restart();
            }
        }
    }
}

void GameManager::initGL()
{
    try
    {
        data = ObjLoader::loadFromFile("dog.obj");
    }
    catch(std::string ex)
    {
        std::cout << "Exception:\n\t" << ex << "!" << std::endl;
    }

    if(!img.loadFromFile("texture.bmp"))
    {
        std::cout << "Failed to load texture!" << std::endl;
    }

    // Set color and depth clear value
    glClearDepth(1.f);
    glClearColor(0.f, 0.f, 0.f, 0.f);

    // Enable Z-buffer read and write
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);

    glEnable(GL_TEXTURE_2D);
    glShadeModel(GL_SMOOTH);

    // Setup a perspective projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(90.f, 1.f, 1.f, 500.f);
}

void GameManager::draw()
{
    static sf::Clock clock;
//    static float x = 0.f, y = 0.f, z = 0.f;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0.f, 0.f, -40.f);

    // Check to see if there are any errors in OpenGL
    if(glGetError())
    {
        std::cout << "OpenGL Error: " << glGetError() << std::endl;
    }

    glRotatef(clock.getElapsedTime().asSeconds() * 10 , 1, 0, 0);
    glRotatef(clock.getElapsedTime().asSeconds() * 10, 0, 1, 0);
    glRotatef(clock.getElapsedTime().asSeconds() * 10, 0, 0, 1);


    for(int x = 0; x < 16; x++)
    {
        for(int y = 0; y < 16; y++)
        {
            for(int z = 0; z < 16; z++)
            {
                glPushMatrix();
                    glTranslatef(x, y, z);
                    RenderUtl::drawCube();
                glPopMatrix();
            }
        }
    }

    glFlush();
}

void GameManager::update()
{
//    MenuManager::getInstance().getCurrentMenu()->update(m_wm);
}
