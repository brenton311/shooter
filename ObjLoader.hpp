#ifndef OBJLOADER_HPP
#define OBJLOADER_HPP

#include <SFML/System.hpp>
#include <vector>
#include <string>

struct FacesVector
{
    unsigned short f1;
    unsigned short f2;
    unsigned short f3;
    unsigned short f4;
};

// Stores the data loaded from the file
struct ObjData
{
    std::vector<sf::Vector3f> vertices;
    std::vector<FacesVector> faces;   // 4 vertices make up a face
};

// Loads the data from the file and stores it in the ObjData struct
class ObjLoader
{
public:
    static ObjData loadFromFile(const std::string& path);
};

#endif // OBJLOADER_HPP
