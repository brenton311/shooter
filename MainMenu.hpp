#ifndef MAINMENU_HPP
#define MAINMENU_HPP

#include "Menu.hpp"

class MainMenu : public Menu
{
    public:
        MainMenu();
        virtual ~MainMenu();
        virtual void draw(WindowManager& wm);
        virtual void update(WindowManager& wm);
};

#endif // MAINMENU_HPP
