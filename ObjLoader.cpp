#include <fstream>
#include <iostream>
#include "ObjLoader.hpp"


ObjData ObjLoader::loadFromFile(const std::string& path)
{
    ObjData data;

    // Open the file and make sure it opened
    std::ifstream file(path.c_str());
    if(!file.good())
    {
        throw "Failed to load OBJ file: " + path + "!";
        //return data;    // Return an empty data object
    }

    // TODO: Implement more error checking
    while(!file.eof())
    {
        std::string temp;
        file >> temp;

        // Load the X, Y, Z vertices for the face
        if(temp == "v")
        {
            sf::Vector3f vertices;
            file >> vertices.x;
            file >> vertices.y;
            file >> vertices.z;
            data.vertices.push_back(vertices);
        }
        // Handle the faces and putting together the vertices
        else if(temp == "f")
        {
            FacesVector faces;
            file >> faces.f1;
            file >> faces.f2;
            file >> faces.f3;
            file >> faces.f4;
            data.faces.push_back(faces);
        }
    }

    file.close();
    return data;
}
