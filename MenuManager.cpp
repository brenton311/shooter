#include "MenuManager.hpp"

MenuManager& MenuManager::getInstance()
{
    static MenuManager mm;
    return mm;
}

void MenuManager::setMenu(Menu* menu)
{
    delete m_menu;
    m_menu = menu;
}

Menu* MenuManager::getCurrentMenu()
{
    return m_menu;
}
