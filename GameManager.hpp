#ifndef GAMEMANAGER_HPP
#define GAMEMANAGER_HPP

#include "WindowManager.hpp"

class GameManager
{
public:
    GameManager();
    ~GameManager();
    void run();
private:
    void initGL();
    void draw();
    void update();
    WindowManager m_wm;
};

#endif // GAMEMANAGER_HPP
