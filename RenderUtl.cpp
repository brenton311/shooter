#include <iostream>
#include <SFML/OpenGL.hpp>
#include "RenderUtl.hpp"


void RenderUtl::renderFromData(ObjData data)
{
    for(unsigned short i = 0; i < data.faces.size(); i++)
    {
        glBegin(GL_QUADS);
            glColor3f(0, 0, 1);
            glVertex3f(data.vertices[data.faces[i].f1 - 1].x, data.vertices[data.faces[i].f1 - 1].y, data.vertices[data.faces[i].f1 - 1].z);
            glVertex3f(data.vertices[data.faces[i].f2 - 1].x, data.vertices[data.faces[i].f2 - 1].y, data.vertices[data.faces[i].f2 - 1].z);
            glVertex3f(data.vertices[data.faces[i].f3 - 1].x, data.vertices[data.faces[i].f3 - 1].y, data.vertices[data.faces[i].f3 - 1].z);
            glVertex3f(data.vertices[data.faces[i].f4 - 1].x, data.vertices[data.faces[i].f4 - 1].y, data.vertices[data.faces[i].f4 - 1].z);
        glEnd();
    }
}

void RenderUtl::drawCube(float x, float y, float z)
{
    // Draw a cube
    glBegin(GL_QUADS);

        // Front Face
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-x, -y,  z);	// Bottom Left Of The Texture and Quad
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( x, -y,  z);	// Bottom Right Of The Texture and Quad
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( x,  y,  z);	// Top Right Of The Texture and Quad
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-x,  y,  z);	// Top Left Of The Texture and Quad
        // Back Face
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-x, -y, -z);	// Bottom Right Of The Texture and Quad
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-x,  y, -z);	// Top Right Of The Texture and Quad
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f( x,  y, -z);	// Top Left Of The Texture and Quad
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f( x, -y, -z);	// Bottom Left Of The Texture and Quad
        // Top Face
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-x, y, -z);	// Top Left Of The Texture and Quad
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-x, y, z);	// Bottom Left Of The Texture and Quad
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( x, y, z);	// Bottom Right Of The Texture and Quad
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( x, y, -z);	// Top Right Of The Texture and Quad
        // Bottom Face
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-x, -y, -z);	// Top Right Of The Texture and Quad
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f( x, -y, -z);	// Top Left Of The Texture and Quad
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f( x, -y,  z);	// Bottom Left Of The Texture and Quad
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-x, -y,  z);	// Bottom Right Of The Texture and Quad
        // Right face
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f( x, -y, -z);	// Bottom Right Of The Texture and Quad
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f( x, y, -z);	// Top Right Of The Texture and Quad
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f( x, y,  z);	// Top Left Of The Texture and Quad
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f( x, -y,  z);	// Bottom Left Of The Texture and Quad
        // Left Face
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-x, -y, -z);	// Bottom Left Of The Texture and Quad
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(-x, -y,  z);	// Bottom Right Of The Texture and Quad
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(-x,  y,  z);	// Top Right Of The Texture and Quad
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(-x,  y, -z);	// Top Left Of The Texture and Quad

    glEnd();
}
