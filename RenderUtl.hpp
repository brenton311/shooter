#ifndef RENDERUTL_HPP
#define RENDERUTL_HPP

#include "ObjLoader.hpp"

namespace RenderUtl
{
    void renderFromData(ObjData data);
    void drawCube(float x = 1.f, float y = 1.f, float z = 1.f);
}

#endif // RENDERUTL_HPP
