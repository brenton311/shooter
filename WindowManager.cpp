#include "WindowManager.hpp"

WindowManager::WindowManager()
    :   m_window(sf::VideoMode(800, 600), "Shooter"), m_active(true)
{
    m_window.setActive(true);
    m_window.setVerticalSyncEnabled(true);
}

WindowManager::~WindowManager()
{
    //dtor
}

sf::RenderWindow* WindowManager::getWindow()
{
    return &m_window;
}

// Swaps the buffers to display what was rendered
void WindowManager::swapBuffers()
{
    m_window.display();
}

bool WindowManager::isOpen()
{
    return m_window.isOpen();
}

void WindowManager::handleEvents()
{
    sf::Event event;
    while(m_window.pollEvent(event))
    {
        if(event.type == sf::Event::Closed)
            m_window.close();

        if(event.type == sf::Event::LostFocus)
            m_active = false;

        if(event.type == sf::Event::GainedFocus)
            m_active = true;

        if(event.type == sf::Event::Resized)
        {
            // update the view to the new size of the window
/*            sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);
            m_window.setView(sf::View(visibleArea));*/
        }
    }

    if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape) && m_active)
        m_window.close();
}

bool WindowManager::isActive()
{
    return m_active;
}
